#pragma once

namespace Lemis{
    enum class ValueType{
        String = 0,
        Boolean,
        Number,
        List,
        Set,
        SortedSet,
        Hash,
        None
    };
}
