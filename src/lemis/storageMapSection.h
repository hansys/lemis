#pragma once
#include <sstream>
#include <vector>
#include <iostream>
#include <string>
#include <map>
#include <optional>
#include <mutex>
#include "../../libs/robin_hood/robin_hood.h"
#include "./values.h"

namespace Lemis{
    class StorageMapSection{
        protected:
            friend class StorageMap;
            robin_hood::unordered_flat_map<std::string,std::unique_ptr<Value>> map;
            bool changed = false;
        public:
            std::mutex mutex_lock;

            explicit StorageMapSection() = default;

            std::optional<Value*> Get(std::string key){
                try{
                    auto value = this->map.at(key).get();
                    if(!value->Expired()){
                        return value;
                    }
                    this->map.erase(key);
                    this->MarkChanged();
                }catch(...){}
                return std::nullopt;
            }

            bool Exists(std::string key){
                return this->map.contains(key);
            }

            bool Set(std::string key, std::unique_ptr<Value> value){
                this->MarkChanged();
                value->SetMapSection(this);
                this->map[key] = std::move(value);
                return true;
            }

            bool Del(std::string key){
                this->MarkChanged();
                return this->map.erase(key) == 1;
            }

            void Clear(){
                this->MarkChanged();
                this->map.clear();
            }

            void MarkUnchanged(){
                this->changed = false;
            }

            void MarkChanged(){
                this->changed = true;
            }

            bool IsChanged();
    };
}