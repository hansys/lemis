#pragma once
#include <sstream>
#include <vector>
#include <iostream>
#include <string>
#include <map>
#include <optional>
#include "../../libs/robin_hood/robin_hood.h"
#include "../../libs/ssr/ssr.h"
#include "./values.h"
#include "./storageMap.h"

namespace Lemis{
    class Storage{
        protected:
            friend class BinarySerializer;
            friend class BinaryDeserializer;
            StorageMap map;

        public:
            explicit Storage() = default;
            Storage(const Storage&) = delete;
            Storage& operator=(Storage x) = delete;

            std::optional<Value*> Get(std::string key){
                return this->map.Get(key);
            }

            template<typename T>
            std::optional<T*> Get(std::string key){
                try{
                    auto item = this->Get(key);
                    auto casted_item = dynamic_cast<T*>(item.value());
                    if(casted_item != nullptr){
                        return casted_item;
                    }
                }catch(...){}
                return std::nullopt;
            }

            bool Exists(std::string key){
                return this->map.Exists(key);
            }

            bool Set(std::string key, const std::string value){
                return this->Set(key, std::make_unique<StringValue>(value));
            }
        
            bool Set(std::string key, const long double value){
                return this->Set(key, std::make_unique<NumberValue>(value));
            }

            bool Set(std::string key, std::unique_ptr<Value> value){
                return this->map.Set(key, std::move(value));
            }

            bool Del(std::string key){
                return this->map.Del(key);
            }

            bool Copy(std::string key, std::string new_key){
                if(!this->Exists(new_key)){
                    auto copied_item = this->Get<StringValue>(key);
                    if(copied_item.has_value()){
                        auto clone = copied_item.value()->Clone();
                        return this->Set(new_key, std::move(clone));
                    }
                }
                return false;
            }

            int Append(std::string key, std::string value){
                auto item = this->Get<StringValue>(key);
                if(item.has_value()){
                    return item.value()->Append(value);
                }
                this->Set(key, value);
                return value.length();
            }

            std::vector<std::string> Keys(std::string search_term = "*"){
                return this->map.Keys(search_term);
            }

            ValueType Type(std::string key){
                auto result = this->Get<Value>(key);
                if(result.has_value()){
                    return result.value()->GetType();
                }
                return ValueType::None;
            }

            size_t TTL(std::string key){
                auto optional_value = this->Get(key);
                if(!optional_value.has_value()){
                    return -2;
                }
                return optional_value.value()->GetTTL();
            }

            bool Expire(std::string key, uint64_t ttl){
                auto optional_value = this->Get(key);
                if(!optional_value.has_value()){
                    return false;
                }
                return optional_value.value()->SetTTL(ttl);
            }

            size_t LPush(std::string key, const std::string value){
                return this->LPush(key, std::make_unique<StringValue>(value));
            }
        
            size_t LPush(std::string key, const long double value){
                return this->LPush(key, std::make_unique<NumberValue>(value));
            }

            size_t LPush(std::string key, std::unique_ptr<Value> value){
                auto optional_list = this->Get<ListValue>(key);
                if(optional_list.has_value()){
                    this->MarkChanged(optional_list.value());
                    return optional_list.value()->Push(std::move(value));
                }
                auto new_list = std::make_unique<ListValue>();
                size_t size = new_list.get()->Push(std::move(value));
                this->Set(key, std::move(new_list));
                return size;
            }

            size_t LRem(std::string key, std::unique_ptr<Value> value){
                return this->LRem(key, value.get()->ToString());
            }

            size_t LRem(std::string key, std::string value){
                auto optional_list = this->Get<ListValue>(key);
                if(!optional_list.has_value()){
                    return 0;
                }
                this->MarkChanged(optional_list.value());
                return optional_list.value()->DelValue(value);
            }

            bool LDel(std::string key){
                auto optional_list = this->Get<ListValue>(key);
                if(!optional_list.has_value()){
                    return false;
                }
                return this->Del(key);
            }

            std::optional<Value*> LIndex(std::string key, size_t index){
                auto optional_list = this->Get<ListValue>(key);
                if(!optional_list.has_value()){
                    return optional_list;
                }
                return optional_list.value()->At(index);
            }

            bool LSet(std::string key, size_t index, std::string value){
                auto optional_list = this->Get<ListValue>(key);
                if(!optional_list.has_value()){
                    return false;
                }
                this->MarkChanged(optional_list.value());
                return optional_list.value()->Set(index, std::make_unique<StringValue>(value));
            }

            size_t LLen(std::string key){
                auto optional_list = this->Get<ListValue>(key);
                if(!optional_list.has_value()){
                    return 0;
                }
                return optional_list.value()->Size();
            }

            std::vector<Value*> LRange(std::string key, int64_t from, int64_t to){
                auto optional_list = this->Get<ListValue>(key);
                if(!optional_list.has_value()){
                    return std::vector<Value*>();
                }
                return optional_list.value()->Range(from, to);
            }

            size_t SAdd(std::string key, const std::string value){
                return this->SAdd(key, std::make_unique<StringValue>(value));
            }
        
            size_t SAdd(std::string key, const long double value){
                return this->SAdd(key, std::make_unique<NumberValue>(value));
            }

            size_t SAdd(std::string key, std::unique_ptr<Value> value){
                auto optional_set = this->Get<SetValue>(key);
                if(optional_set.has_value()){
                    this->MarkChanged(optional_set.value());
                    return optional_set.value()->Add(std::move(value));
                }
                auto new_set = std::make_unique<SetValue>();
                size_t size = new_set.get()->Add(std::move(value));
                this->Set(key, std::move(new_set));
                return size;
            }

            size_t SRem(std::string key, std::unique_ptr<Value> value){
                return this->LRem(key, value.get()->ToString());
            }

            size_t SRem(std::string key, std::string value){
                auto optional_set = this->Get<SetValue>(key);
                if(!optional_set.has_value()){
                    return 0;
                }
                this->MarkChanged(optional_set.value());
                return optional_set.value()->Del(value);
            }

            size_t SIsMember(std::string key, std::string value){
                auto optional_set = this->Get<SetValue>(key);
                if(!optional_set.has_value()){
                    return 0;
                }
                return optional_set.value()->Contains(value);
            }


            bool SMove(std::string old_key, std::string new_key, std::string value){
                auto optional_set = this->Get<SetValue>(old_key);
                if(!optional_set.has_value()){
                    return false;
                }
                optional_set.value()->Del(value);
                this->MarkChanged(optional_set.value());
                return this->SAdd(new_key, value);
            }

            bool SDel(std::string key){
                auto optional_list = this->Get<SetValue>(key);
                if(!optional_list.has_value()){
                    return false;
                }
                return this->Del(key);
            }


            size_t SLen(std::string key){
                auto optional_set = this->Get<SetValue>(key);
                if(!optional_set.has_value()){
                    return 0;
                }
                return optional_set.value()->Size();
            }

            bool FlushAll(){
                return this->map.Clear();
            }

            bool IsChanged(){
                return this->map.IsChanged();
            }

            void MarkChanged(Value* value){
                if(value != nullptr && value->GetMapSection() != nullptr){
                    value->GetMapSection()->MarkChanged();
                }
            }

            void MarkUnChanged(){
                return this->map.MarkUnChanged();
            }

            std::mutex& GetLock(std::string key){
                return this->map.GetLock(key);
            }
    };
}