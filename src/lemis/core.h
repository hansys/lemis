#pragma once
#include "./storage.h"
#include "./config.h"

namespace Lemis{
    class Core{
        Storage storage;
        public:
            Core() = default;

            Storage* GetStorage(){
                return &this->storage;
            }
    };
}
