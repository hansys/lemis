#pragma once
#include <string>
#include <thread>
#include "./config.h"		

namespace Lemis{
    class ConfigParser{
        protected:
            static void SaveToVariable(Config& config, std::string key, std::string value){
                try{
                    if(key == "threads"){
                        auto threads = std::stoi(value);
                        config.threads = threads > 0 ? threads : config.threads;
                        return;
                    }
                    if(key == "name"){
                        config.name = value;
                        return;
                    }
                    if(key == "address" || key == "bind"){
                        config.address = value;
                        return;
                    }
                    if(key == "port"){
                        auto port = std::stoi(value);
                        config.port = port <= 65535 ? port : config.port;
                        return;
                    }
                    if(key == "timeout"){
                        auto timeout = std::stoi(value);
                        config.timeout = timeout > 0 ? timeout : config.timeout;
                        return;
                    }
                    if(key == "requirePassword"){
                        config.requirePassword = value == "true";
                        return;
                    }
                    if(key == "password"){
                        config.password = value;
                        return;
                    }
                    if(key == "storageFileLocation" || key == "file"){
                        config.storageFileLocation = value;
                        return;
                    }
                    if(key == "persistentStorage" || key == "persistent"){
                        config.persistentStorage = value == "true";
                        return;
                    }
                    if(key == "storageSaveEachSec" || key == "save"){
                        auto interval = std::stoi(value);
                        config.storageSaveEachSec = interval > 0 ? interval : config.storageSaveEachSec;
                        return;
                    }
                }catch(...){}
            }
        public:
            static Config ParseFile(std::string fileLocation){
                Config config;
                try{
                    std::ifstream fileStream;
                    std::string line;
                    fileStream.open(fileLocation);
                    if(fileStream.is_open()){
                        while(std::getline(fileStream, line)){
                            auto parts = SplitStringByChar(line, ' ');
                            if(parts.size() >= 2){
                                std::string key = parts.at(0);
                                std::string value = parts.at(1);
                                SaveToVariable(config, key, value);
                            }
                        }
                        fileStream.close();
                        return config;
                    }
                }catch(...){}
                std::cout << "ERROR: Couldn't load configuration file." <<"\n";
                return config;
            }

            static std::vector<std::string> SplitStringByChar(std::string text, char delimeter){
                std::stringstream ss(text);
                std::string part;
                std::vector<std::string> parts;
                while(std::getline(ss, part, delimeter))
                {
                    parts.push_back(part);
                }
                return parts;
            }
    };
}