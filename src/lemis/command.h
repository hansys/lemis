#pragma once
#include <vector>
#include <string>
#include <functional>
#include "./core.h"

namespace Lemis{
    class Command{
        public:
            std::string name;
            std::string sub_name;
            std::vector<std::string> params;
    };
}