#pragma once
#include <string>
#include <thread>
		
namespace Lemis{
    class Config{
        public:
            uint32_t threads = std::thread::hardware_concurrency();

            std::string name = "Lemis";
            std::string address = { "127.0.0.1" };
            uint16_t port = 6379;
            uint32_t timeout = 300;

            bool requirePassword = false;
            std::string password = "";

            std::string storageFileLocation = "./data.bin";
            bool persistentStorage = true;
            uint32_t storageSaveEachSec = 3;

            std::string GetBindAddress(){
                return address + ":" + std::to_string(port);
            }
    };
}