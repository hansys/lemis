#pragma once
#include <iostream>
#include "../values.h"

namespace Lemis{
    class Storage;
    class StorageMap;
    
    class Serializer{
        protected:
            std::ostream& os;
        public:
            Serializer(std::ostream& os) : os(os){

            }

            virtual void SerializeValue(Value* value) = 0;
            virtual void SerializeValue(BooleanValue* value) = 0;
            virtual void SerializeValue(StringValue* value) = 0;
            virtual void SerializeValue(NumberValue* value) = 0;
            virtual void SerializeValue(ListValue* value) = 0;
            virtual void SerializeValue(SetValue* value) = 0;
            virtual void SerializeStorageMap(StorageMap* map) = 0;
            virtual void SerializeStorage(Storage* storage) = 0;
    };
}