#pragma once
#include <vector>
#include <string>
#include <iostream>
#include "../valueType.h"
#include "./deserializer.h"

namespace Lemis{

    class BinaryDeserializer : public Deserializer {
        public:
            BinaryDeserializer(std::istream& is) : Deserializer(is) {

            }
            void DeserializeLongDouble(long double& num){
                is.read(reinterpret_cast<char*>(&num), sizeof(num));
                CheckEndOfStream(is);
            }

            void DeserializeUint(uint32_t& num){
                is.read(reinterpret_cast<char*>(&num), sizeof(num));
                CheckEndOfStream(is);
            }

            void DeserializeValueType(ValueType& type){
                is.read(reinterpret_cast<char*>(&type), sizeof(type));
                CheckEndOfStream(is);
            }

            void DeserializeLong(long& num){
                is.read(reinterpret_cast<char*>(&num), sizeof(num));
                CheckEndOfStream(is);
            }

            void DeserializeBool(bool& val){
                is.read(reinterpret_cast<char*>(&val), sizeof(val));
                CheckEndOfStream(is);
            }

            void DeserializeString(std::string& string){
                size_t string_size = 0;
                is.read(reinterpret_cast<char*>(&string_size), sizeof(string_size));
                CheckEndOfStream(is);
                string.resize(string_size);
                is.read(reinterpret_cast<char*>(&string[0]), string_size);
                CheckEndOfStream(is);
            }

            void DeserializeValue(Value* value){
                DeserializeLong(value->expiration);
            }

            void DeserializeValue(BooleanValue* value){
                DeserializeBool(value->value);
            }

            void DeserializeValue(StringValue* value){
                DeserializeString(value->value);
            }

            void DeserializeValue(NumberValue* value){
                DeserializeLongDouble(value->value);
            }

            void DeserializeValue(ListValue* value){
                long size = 0;
                DeserializeLong(size);
                for(long i = 0; i < size; i++){
                    auto optional_value = DeserializeValueInstance();
                    if(optional_value.has_value()){
                        value->Push(std::move(optional_value.value()));
                    }
                }
            }

            void DeserializeValue(SetValue* value){
                long size = 0;
                DeserializeLong(size);
                for(long i = 0; i < size; i++){
                    auto optional_value = DeserializeValueInstance();
                    if(optional_value.has_value()){
                        value->Add(std::move(optional_value.value()));
                    }
                }
            }

            std::optional<std::unique_ptr<Value>> DeserializeValueInstance(){
                ValueType type;
                DeserializeValueType(type);
                if(type == ValueType::Boolean){
                    auto value = std::make_unique<BooleanValue>();
                    DeserializeValue(static_cast<Value*>(value.get()));
                    DeserializeValue(value.get());
                    return value;
                }
                if(type == ValueType::String){
                    auto value = std::make_unique<StringValue>();
                    DeserializeValue(static_cast<Value*>(value.get()));
                    DeserializeValue(value.get());
                    return value;
                }
                if(type == ValueType::Number){
                    auto value = std::make_unique<NumberValue>();
                    DeserializeValue(static_cast<Value*>(value.get()));
                    DeserializeValue(value.get());
                    return value;
                }
                if(type == ValueType::List){
                    auto value = std::make_unique<ListValue>();
                    DeserializeValue(static_cast<Value*>(value.get()));
                    DeserializeValue(value.get());
                    return value;
                }
                if(type == ValueType::Set){
                    auto value = std::make_unique<SetValue>();
                    DeserializeValue(static_cast<Value*>(value.get()));
                    DeserializeValue(value.get());
                    return value;
                }
                return std::nullopt;
            }

            void DeserializeStorageMap(StorageMap* map){
                long size = 0;
                DeserializeLong(size);
                for(long i = 0; i < size; i++){
                    std::string key;
                    DeserializeString(key);
                    auto optional_value = DeserializeValueInstance();
                    if(optional_value.has_value()){
                        map->Set(key, std::move(optional_value.value()));
                    }
                }
            }

            void DeserializeStorage(Storage* storage){
                DeserializeStorageMap(&storage->map);
            }

            void CheckEndOfStream(std::istream& is){
                if(is.eof()){
                    throw "Empty stream exception";
                }
            }


    };
}