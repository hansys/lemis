#pragma once
#include <vector>
#include <string>
#include <iostream>
#include "../valueType.h"
#include "../values.h"
#include "../storageMap.h"
#include "./serializer.h"

namespace Lemis{
    class BinarySerializer : public Serializer {
        public:
            BinarySerializer(std::ostream& os) : Serializer(os){

            }

            void SerializeLongDouble(long double num){
                os.write(reinterpret_cast<const char*>(&num), sizeof(num));
            }

            void SerializeUint(uint32_t num){
                os.write(reinterpret_cast<const char*>(&num), sizeof(num));
            }

            void SerializeValueType(ValueType type){
                os.write(reinterpret_cast<const char*>(&type), sizeof(type));
            }

            void SerializeLong(long num){
                os.write(reinterpret_cast<const char*>(&num), sizeof(num));
            }

            void SerializeBool(bool val){
                os.write(reinterpret_cast<const char*>(&val), sizeof(val));
            }

            void SerializeString(std::string& string){
                size_t string_size = string.size();
                os.write(reinterpret_cast<const char*>(&string_size), sizeof(string_size));
                os.write(string.data(), string_size);
            }

            void SerializeValue(Value* value){
                SerializeValueType(value->type);
                SerializeLong(value->expiration);
                if(value->GetType() == ValueType::Boolean){
                    SerializeValue(dynamic_cast<NumberValue*>(value));
                    return;
                }
                if(value->GetType() == ValueType::String){
                    SerializeValue(dynamic_cast<StringValue*>(value));
                    return;
                }
                if(value->GetType() == ValueType::Number){
                    SerializeValue(dynamic_cast<NumberValue*>(value));
                    return;
                }
                if(value->GetType() == ValueType::List){
                    SerializeValue(dynamic_cast<ListValue*>(value));
                    return;
                }
                if(value->GetType() == ValueType::Set){
                    SerializeValue(dynamic_cast<SetValue*>(value));
                    return;
                }
            }

            void SerializeValue(BooleanValue* value){
                SerializeBool(value->value);
            }

            void SerializeValue(StringValue* value){
                SerializeString(value->value);
            }

            void SerializeValue(NumberValue* value){
                SerializeLongDouble(value->value);
            }

            void SerializeValue(ListValue* value){
                SerializeLong(value->list.size());
                for(auto& value_ptr : value->list){
                    SerializeValue(value_ptr.get());
                }
            }

            void SerializeValue(SetValue* value){
                SerializeLong(value->set.size());
                for(auto& value_ptr : value->set){
                    SerializeValue(value_ptr.get());
                }
            }

            void SerializeStorageMap(StorageMap* map){
                auto keys = map->Keys("*");
                SerializeLong(keys.size());
                for(auto key : keys){
                    std::unique_lock<std::mutex> lock(map->GetLock(key));
                    auto optional_value = map->Get(key);
                    if(optional_value.has_value()){
                        SerializeString(key);
                        SerializeValue(optional_value.value());
                    }
                }
            }

            void SerializeStorage(Storage* storage){
                SerializeStorageMap(&storage->map);
            }
    };
}