#pragma once
#include <iostream>
#include "../values.h"

namespace Lemis{
    class Storage;
    class StorageMap;

    class Deserializer{
        protected:
            std::istream& is;
        public:
            Deserializer(std::istream& is) : is(is){

            }

            virtual void DeserializeValue(Value* value) = 0;
            virtual void DeserializeValue(BooleanValue* value) = 0;
            virtual void DeserializeValue(StringValue* value) = 0;
            virtual void DeserializeValue(NumberValue* value) = 0;
            virtual void DeserializeValue(ListValue* value) = 0;
            virtual void DeserializeValue(SetValue* value) = 0;
            virtual void DeserializeStorageMap(StorageMap* map) = 0;
            virtual void DeserializeStorage(Storage* storage) = 0;

    };
}