#pragma once
#include <sstream>
#include <vector>
#include <ostream>
#include <string>
#include <mutex>
#include <chrono>
#include <set>
#include "./valueType.h"

namespace Lemis{
    class StorageMapSection;

    class Value{
        protected:
            friend class BinarySerializer;
            friend class BinaryDeserializer;
            ValueType type;
            time_t expiration;
            StorageMapSection* mapSection;
            virtual Value* CloneInternal() const = 0;
        public:
            Value(ValueType type) : type(type), expiration(0), mapSection(nullptr){

            }

            ValueType GetType(){
                return this->type;
            }
            
            virtual ~Value() = default;

            std::unique_ptr<Value> Clone() const{
                return std::unique_ptr<Value>(this->CloneInternal());
            };

            virtual std::string ToString() const = 0;

            bool Expired(){
                if(this->expiration == 0){
                    return false;
                }
                return this->expiration < this->time_since_epoch();
            }

            time_t time_since_epoch(){
                auto now = std::chrono::system_clock::now();
                return std::chrono::system_clock::to_time_t(now);
            }

            bool SetTTL(time_t ttl){
                this->expiration = this->time_since_epoch() + ttl;
                return true;
            }

            uint64_t GetTTL(){
                if(this->expiration == 0){
                    return -1;
                }
                return this->expiration - this->time_since_epoch();
            }

            void SetMapSection(StorageMapSection* mapSection){
                this->mapSection = mapSection;
            }

            StorageMapSection* GetMapSection(){
                return this->mapSection;
            }
    };

    class StringValue : public Value {
        protected:
            friend class BinarySerializer;
            friend class BinaryDeserializer;
            std::string value;
            StringValue* CloneInternal() const override
            {
                return new StringValue(this->value);
            }
        public:
            StringValue() : Value(ValueType::String){

            }

            StringValue(std::string value) : Value(ValueType::String), value(value){

            }

            int Append(std::string value){
                this->value += value;
                return this->value.length();
            }

            std::unique_ptr<StringValue> Clone() const{
                return std::unique_ptr<StringValue>(this->CloneInternal());
            }

            std::string ToString() const{
                return this->value;
            }

            const std::string& GetData() const{
                return this->value;
            }
            StringValue* GetSelf(){
                return this;
            }
    };

    class BooleanValue : public Value {
        protected:
            friend class BinarySerializer;
            friend class BinaryDeserializer;
            bool value;
            BooleanValue* CloneInternal() const override
            {
                return new BooleanValue(this->value);
            }

        public:
            BooleanValue() : Value(ValueType::Boolean){

            }
            BooleanValue(bool value) : Value(ValueType::Boolean), value(value){

            }

            std::unique_ptr<BooleanValue> Clone() const{
                return std::unique_ptr<BooleanValue>(this->CloneInternal());
            }

            std::string ToString() const{
                return std::to_string(this->value);
            }

            bool GetData() const{
                return this->value;
            }
            virtual BooleanValue* GetSelf(){
                return this;
            }
    };

    class NumberValue : public Value {
        protected:
            friend class BinarySerializer;
            friend class BinaryDeserializer;
            long double value;
            NumberValue* CloneInternal() const override
            {
                return new NumberValue(this->value);
            }

        public:
            NumberValue() : Value(ValueType::Number){

            }
            NumberValue(long double value) : Value(ValueType::Number), value(value){

            }

            std::unique_ptr<NumberValue> Clone() const{
                return std::unique_ptr<NumberValue>(this->CloneInternal());
            }

            std::string ToString() const{
                return std::to_string(this->value);
            }

            long double GetData() const{
                return this->value;
            }
            virtual NumberValue* GetSelf(){
                return this;
            }
    };

    class ListValue : public Value {
        protected:
            friend class BinarySerializer;
            friend class BinaryDeserializer;
            std::vector<std::unique_ptr<Value>> list;

            ListValue* CloneInternal() const override
            {
                return new ListValue(this->list);
            }

        public:
            ListValue() : Value(ValueType::List){

            }
            ListValue(const std::vector<std::unique_ptr<Value>>& list) : ListValue() {
                for(auto& item : list){
                    this->list.push_back(
                        std::move(item.get()->Clone())
                    );
                }
            }

            std::unique_ptr<ListValue> Clone() const{
                return std::unique_ptr<ListValue>(this->CloneInternal());
            }

            std::string ToString() const{
                std::string output;
                for(auto& value : list){    
                    output += value->ToString();
                }
                return output;
            }

            size_t Size(){
                return this->list.size();
            }

            size_t Push(std::unique_ptr<Value> value){
                this->list.push_back(
                    std::move(value)
                );
                return this->list.size();
            }

            size_t DelValue(Value* value){
                if(value == nullptr){
                    return 0;
                }
                return this->DelValue(value->ToString());
            }

            size_t DelValue(std::string value){
                size_t count = 0;
                if(this->Size() > 0){
                    for(int64_t i = this->list.size() -1; i >= 0; i--){
                        if(this->list.at(i).get()->ToString() == value){
                            this->list.erase(this->list.begin() + i);
                            count++;
                        }
                    }
                }
                return count;
            }

            size_t DelIndex(size_t index){
                if(index >= this->Size()){
                    return 0;
                }
                this->list.erase(this->list.begin() + index);
                return 1;
            }

            bool Set(size_t index, std::unique_ptr<Value> value){
                if(index >= this->Size()){
                    return false;
                }
                this->list[index] = std::move(value);
                return true;
            }

            std::optional<Value*> At(size_t index){
                if(index >= this->Size()){
                    return std::nullopt;
                }
                return this->list.at(index).get();
            }

            const std::vector<std::unique_ptr<Value>>& GetData() const{
                return this->list;
            }

            std::vector<Value*> Range(int64_t from, int64_t to){
                std::vector<Value*> values;
                int64_t size = static_cast<int64_t>(this->Size());
                from = from < 0 ? this->Size() - from : from;
                to = to < 0 ? this->Size() - to : to;
                to = to >= size ? size-1 : to;
                if(from > to || to >= size || from < 0 || to < 0){
                    return values;
                }
                for(int64_t i = from; i <= to; i++){
                    values.push_back(
                        this->list.at(i).get()
                    );
                }
                return values;
            }
            virtual ListValue* GetSelf(){
                return this;
            }
    };

    class SetValue : public Value {
        protected:
            friend class BinarySerializer;
            friend class BinaryDeserializer;
            std::set<std::unique_ptr<Value>> set;

            SetValue* CloneInternal() const override
            {
                return new SetValue(this->set);
            }

        public:
            SetValue() : Value(ValueType::Set){

            }
            SetValue(const std::set<std::unique_ptr<Value>>& set) : SetValue() {
                for(auto& item : set){
                    this->set.insert(
                        std::move(item.get()->Clone())
                    );
                }
            }

            size_t Add(std::unique_ptr<Value> value){
                this->set.insert(
                    std::move(value)
                );
                return 1;
            }

            size_t Del(std::string value){
                for(auto& item : this->set){
                    if(item->ToString() == value){
                        this->set.erase(item);
                        return 1;
                    }
                }
                return 0;
            }

            bool Contains(std::string value){
                for(auto& item : this->set){
                    if(item->ToString() == value){
                        return true;
                    }
                }
                return false;
            }

            std::unique_ptr<SetValue> Clone() const{
                return std::unique_ptr<SetValue>(this->CloneInternal());
            }

            std::string ToString() const{
                std::string output;
                for(auto& value : set){    
                    output += value->ToString();
                }
                return output;
            }

            size_t Size(){
                return this->set.size();
            }

            const std::set<std::unique_ptr<Value>>& GetData() const{
                return this->set;
            }
            virtual SetValue* GetSelf(){
                return this;
            }
        };


    std::ostream &operator<<(std::ostream &os, const Value* value){
        os << value->ToString();
        return os;

    }
}