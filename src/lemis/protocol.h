#pragma once
#include <string>
#include <memory>
#include "./core.h"
#include "./command.h"

namespace Lemis{
    class Protocol{
        protected:
            std::shared_ptr<Lemis::Core> lemis;
        public:
            Protocol(std::shared_ptr<Lemis::Core> lemis) : lemis(lemis){}
            virtual Lemis::Command Parse(std::string& input) = 0;
            virtual std::string Exec(Lemis::Command& command) = 0;
            virtual ~Protocol() = default;
            Storage* GetStorage(){
                return this->lemis->GetStorage();
            }
    };
}

