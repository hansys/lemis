#pragma once
#include <sstream>
#include <vector>
#include <iostream>
#include <string>
#include <map>
#include <functional>
#include <optional>
#include <mutex>
#include "../../libs/robin_hood/robin_hood.h"
#include "./values.h"
#include "./storageMapSection.h"

namespace Lemis{
    class StorageMap{
        protected:
            size_t size;
            std::hash<std::string> hash_fn;
            std::unique_ptr<StorageMapSection[]> sections;
        public:
            explicit StorageMap(size_t size = 1000) : size(size), sections(std::make_unique<StorageMapSection[]>(size)) {

            };

            std::optional<Value*> Get(std::string key){
                return this->sections[this->GetIndex(key)].Get(key);
            }

            bool Set(std::string key, std::unique_ptr<Value> value){
                return this->sections[this->GetIndex(key)].Set(key, std::move(value));
            }

            bool Exists(std::string key){
                return this->sections[this->GetIndex(key)].Exists(key);
            }

            bool Del(std::string key){
                return this->sections[this->GetIndex(key)].Del(key);
            }

            bool Clear(){
                for(size_t i = 0; i < this->size; i++){
                    std::unique_lock<std::mutex> lock(this->sections[i].mutex_lock);
                    this->sections[i].Clear();
                }
                return true;
            }

            std::vector<std::string> Keys(std::string search_term){
                ssr::Regex regex(GetRegexMatcher(search_term));
                std::vector<std::string> keys;
                for(size_t i = 0; i < this->size; i++){
                    auto& section = this->sections[i];
                    std::unique_lock<std::mutex> lock(section.mutex_lock);
                    for(auto& pair : section.map){
                        auto value = section.Get(pair.first).value();
                        if(value->Expired()){
                            section.Del(pair.first);
                            continue;
                        }
                        if (search_term == "*" || regex.match(pair.first)) {
                            keys.push_back(pair.first);
                        }
                    }
                }
                return keys;
            }

            bool IsChanged(){
                std::vector<std::string> keys;
                for(size_t i = 0; i < this->size; i++){
                    auto& section = this->sections[i];
                    std::unique_lock<std::mutex> lock(section.mutex_lock);
                    if(section.changed){
                        return true;
                    }
                }
                return false;
            }

            void MarkUnChanged(){
                for(size_t i = 0; i < this->size; i++){
                    auto& section = this->sections[i];
                    std::unique_lock<std::mutex> lock(section.mutex_lock);
                    section.MarkUnchanged();
                }
            }

            std::mutex& GetLock(std::string key){
                return this->sections[this->GetIndex(key)].mutex_lock;
            }

            
            size_t GetIndex(std::string key){
                auto hash_value = static_cast<size_t>(hash_fn(key));
                return hash_value % this->size;
            }
            
           
            /*
            size_t GetIndex(std::string key){
                size_t index = key.length();
                index += key.length() >= 2 ? static_cast<size_t>(key[1]) : 0;
                index += key.length() >= 1 ? static_cast<size_t>(key[0]) : 0;
                return index % this->size;
            }
            */

            static std::string ReplaceAll(const std::string & str, const std::string & find, const std::string & replace) {
                std::string result;
                size_t find_len = find.size();
                size_t pos;
                size_t from=0;
                while (std::string::npos != (pos = str.find(find,from))) {
                    result.append(str, from, pos - from);
                    result.append(replace);
                    from = pos + find_len;
                }
                result.append(str, from, std::string::npos);
                return result;
            }

            static std::string GetRegexMatcher(const std::string& search_term){
                auto replaced_search_term = StorageMap::ReplaceAll(search_term, "*", ".*");
                replaced_search_term = StorageMap::ReplaceAll(replaced_search_term, "?", ".");
                return replaced_search_term;
            }
    };
}

