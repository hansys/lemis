#pragma once
#include <vector>
#include <string>
#include <filesystem>

#include "./serializers/binarySerializer.h"
#include "./serializers/binaryDeserializer.h"

#include "../../libs/evpp/tcp_server.h"
#include "../../libs/evpp/buffer.h"
#include "../../libs/evpp/tcp_conn.h"
#include "../../libs/evpp/timestamp.h"

#include "./core.h"
#include "./config.h"
#include "./protocol.h"

namespace Lemis{
    class Server{
        protected:
            std::unique_ptr<Lemis::Protocol> protocol;
            Config config;
            evpp::EventLoop loop;
            evpp::TCPServer server;
        public:
            Server(const Server&) = delete;
            Server& operator=(Server x) = delete;

            Server(std::unique_ptr<Lemis::Protocol> protocol, Config config) : protocol(std::move(protocol)), config(config), server(&loop, config.GetBindAddress(), config.name, config.threads) {
                
                server.SetThreadDispatchPolicy(
                    evpp::ThreadDispatchPolicy::Policy::kRoundRobin
                );

                server.SetMessageCallback([this](const evpp::TCPConnPtr& conn, evpp::Buffer* msg){
                    std::string input = msg->NextAllString();
                    auto command = this->protocol->Parse(input);
                    auto result = this->protocol->Exec(command);
                    conn->Send(result);
                });

                server.SetConnectionCallback([](const evpp::TCPConnPtr& conn) {
                    if (conn->IsConnected()) {
                        //std::cout << "INFO: Connected from " << conn->remote_addr() << "\n";
                    } else {
                        //std::cout << "INFO: Lost connection from " << conn->remote_addr() << "\n";
                    }
                });

                if(config.persistentStorage){
                    int64_t interval = config.storageSaveEachSec * evpp::Duration::kSecond;
                    loop.RunEvery(evpp::Duration(interval), [this](){
                        if(this->protocol->GetStorage()->IsChanged()){
                            this->Save();
                        }
                    });
                }

                server.Init();
            }

            bool Save(){
                try{
                    std::ofstream fileStream;
                    std::string tmpFileLocation = config.storageFileLocation + ".tmp";
                    fileStream.open(tmpFileLocation);
                    if(fileStream.is_open()){
                        BinarySerializer serializer(fileStream);
                        serializer.SerializeStorage(this->protocol->GetStorage());
                        fileStream.close();
                        if (!std::filesystem::exists(config.storageFileLocation) || std::filesystem::remove(config.storageFileLocation)){
                            std::filesystem::rename(tmpFileLocation, config.storageFileLocation);
                            this->protocol->GetStorage()->MarkUnChanged();
                            return true;
                        }                    
                    }
                }catch(...){}
                std::cout << "ERROR: Couldn't save data.\n";
                return false;
            }

            bool Load(){
                try{
                    std::ifstream fileStream;
                    fileStream.open(config.storageFileLocation);
                    if(fileStream.is_open()){
                        std::cout << "INFO: Loading data...\n";
                        BinaryDeserializer deserializer(fileStream);
                        deserializer.DeserializeStorage(this->protocol->GetStorage());
                        fileStream.close();
                        return true;
                    }
                }catch(...){}
                std::cout << "ERROR: Couldn't load data.\n";
                return false;
            }

            void Start(){
                std::cout << "INFO: Starting Lemis server with number of threads: " << this->config.threads << "\n";
                std::cout << "INFO: Server is listening on " << this->config.GetBindAddress() << "\n";
                this->server.Start();
                this->loop.Run();
            }

            void Stop(){
                this->server.Stop();
            }

            void Exit(){
                if(config.persistentStorage){
                    this->Save();
                }
            }
    };
}