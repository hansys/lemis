#pragma once
#include <sstream>
#include <vector>
#include <iostream>
#include <string>
#include "./mapper.h"
#include "./formatter.h"
#include "../lemis/protocol.h"
#include "../lemis/values.h"

namespace Resp2{
    class Protocol : public Lemis::Protocol{
        public:
            Protocol(std::shared_ptr<Lemis::Core> lemis) : Lemis::Protocol(lemis){}

            Lemis::Command Parse(std::string& input){
                Lemis::Command command;
                bool name_found = false;
                bool sub_name_found = false;

                size_t size = input.size();
                for(size_t i = 0; i < size; i++){
                    if(input[i] == '$'){
                        uint32_t parts = 0;
                        try{
                            i++;
                            while(input[i] != '\r') {
                                parts = (parts*10)+(input[i] - '0');
                                i += i + 1 < size ? 1 : 0;
                            }
                            i += i + 2 < size ? 2 : 0; 
                            std::string buffer;
                            for(uint32_t j = 0; j < parts && i < size; j++){
                                buffer += input[i];
                                i += i + 1 < size ? 1 : 0;
                            }
                            if(!name_found){
                                command.name = Formatter::StringToLower(buffer);
                                name_found = true;
                                continue;
                            }
                            if(!sub_name_found){
                                command.sub_name = Formatter::StringToLower(buffer);
                                sub_name_found = true;
                            }
                            command.params.push_back(buffer);
                        }catch(...){}          
                    }
                }
                return command;
            }


            std::string Exec(Lemis::Command& command){
                if(command.name.length() > 0){
                    auto function_opt = Mapper::Map(command);   
                    if(function_opt.has_value()){
                        auto function = function_opt.value();
                        return function(this->lemis.get(), command);
                    }
                }
                return Formatter::ErrorCommand(command);
            }

    };
}