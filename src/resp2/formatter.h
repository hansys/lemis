#pragma once
#include <sstream>
#include <vector>
#include <iostream>
#include <string>
#include <functional>
#include <unordered_map>
#include <set>
#include "../lemis/values.h"

namespace Resp2{
    class Formatter{
        public:
            static std::string Format(const std::string& value){
                return "$" + std::to_string(value.length()) + "\r\n" + value + "\r\n";
            }
            static std::string Format(std::vector<std::string> values){
                std::string output = "*" + std::to_string(values.size()) + "\r\n";
                for(auto& value : values){
                    output += Format(value);
                }
                return output;
            }

            static std::string Format(std::vector<Lemis::Value*> values){
                std::string output = "*" + std::to_string(values.size()) + "\r\n";
                for(auto* value : values){
                    output += Format(value->ToString());
                }
                return output;
            }
            static std::string Format(const std::set<std::unique_ptr<Lemis::Value>>& values){
                std::string output = "*" + std::to_string(values.size()) + "\r\n";
                for(auto& value : values){
                    output += Format(value.get()->ToString());
                }
                return output;
            }
            static std::string Format(const std::vector<std::unique_ptr<Lemis::Value>>& values){
                std::string output = "*" + std::to_string(values.size()) + "\r\n";
                for(auto& value : values){
                    output += Format(value.get()->ToString());
                }
                return output;
            }
            static std::string Format(const Lemis::StringValue* value){
                return Format(value->GetData());
            }
            static std::string Format(const Lemis::ListValue* value){
                return Format(value->GetData());
            }
            static std::string Format(const Lemis::SetValue* value){
                return Format(value->GetData());
            }
            static std::string Format(const Lemis::Value* value){
                return Format(value->ToString());
            }
            static std::string Format(int value) {
                std::string output = ":";
                output += std::to_string(value);
                output += "\r\n";
                return output;
            }
            static std::string FormatScan(size_t index, std::vector<std::string> values){
                std::string output = "*2\r\n";
                output += Formatter::Format(std::to_string(index));
                output += Formatter::Format(values);
                return output;
            }
            static std::string Format(Lemis::ValueType type){
                if(type == Lemis::ValueType::String){
                    return Formatter::Format(std::string("string"));
                }
                if(type == Lemis::ValueType::List){
                    return Formatter::Format(std::string("list"));
                }
                if(type == Lemis::ValueType::Hash){
                    return Formatter::Format(std::string("hash"));
                }
                if(type == Lemis::ValueType::Number){
                    return Formatter::Format(std::string("integer"));
                }
                if(type == Lemis::ValueType::Boolean){
                    return Formatter::Format(std::string("boolean"));
                }
                if(type == Lemis::ValueType::Set){
                    return Formatter::Format(std::string("set"));
                }
                if(type == Lemis::ValueType::SortedSet){
                    return Formatter::Format(std::string("sortedset"));
                }
                return Formatter::Format(std::string("none"));
            }
            static std::string ErrorCommand(const Lemis::Command& cmd){
                std::string output = "-ERR unknown command `" + cmd.name + "`, with args beginning with: ";
                for(const auto& param : cmd.params){
                    output += "`" + param + "`, ";
                }
                output += "\r\n";
                return output;
            }
            static std::string ErrorParams(const Lemis::Command& cmd) {
                return "-ERR wrong number of arguments for `" + cmd.name + "` command\r\n";
            }
            static std::string Error(std::string message = "") {
                return "-" + message + "\r\n";
            }
            static std::string Echo(std::string message = "") {
                return "+" + message + "\r\n";
            }
            static std::string Nil() {
                return "$-1\r\n";
            }
            static std::string Pong() {
                return "+pong\r\n";
            }
            static std::string Ok() {
                return "+ok\r\n";
            }

            static std::string& StringToLower(std::string& string){
                for(auto& symbol : string){
                    symbol = std::tolower(symbol);
                }
                return string;
            }
    };
}