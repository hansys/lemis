#pragma once
#include <sstream>
#include <vector>
#include <iostream>
#include <string>
#include <functional>
#include <unordered_map>
#include "../lemis/protocol.h"
#include "../lemis/values.h"
#include "../lemis/core.h"
#include "./formatter.h"

namespace Resp2{
    typedef std::function<std::string(Lemis::Core*, Lemis::Command&)> Function;

    class Mapper{
        protected:
            static std::unordered_map<std::string, Function> functions;
        public:
            Mapper() = delete;

            static std::optional<Function> Map(Lemis::Command& cmd){
                try{
                    return functions.at(cmd.name);
                }catch(...){}
                return std::nullopt;
            }

            static std::string Get(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->Get<Lemis::StringValue>(cmd.params.at(0));
                if(output.has_value()){
                    return Formatter::Format(output.value());
                }
                return Formatter::Nil();
            }

            static std::string Set(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 2){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->Set(cmd.params.at(0), cmd.params.at(1));
                return Formatter::Format(output);    
            }

            static std::string Del(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);                    
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->Del(cmd.params.at(0));
                return Formatter::Format(output);
            }

            static std::string Copy(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 2){
                    return Formatter::ErrorParams(cmd);   
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->Copy(cmd.params.at(0), cmd.params.at(1));
                return Formatter::Format(output);   
            }

            static std::string Append(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 2){
                    return Formatter::ErrorParams(cmd); 
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->Append(cmd.params.at(0), cmd.params.at(1));
                return Formatter::Format(output);
            }

            static std::string Exists(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() < 1){
                    return Formatter::ErrorParams(cmd);
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                size_t output = 0;
                for(size_t i = 0; i < cmd.params.size(); i++){
                    output += core->GetStorage()->Exists(cmd.params.at(i)) ? 1 : 0;
                }
                return Formatter::Format(output);
            }

            static std::string Keys(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);
                }
                return Formatter::Format(
                     core->GetStorage()->Keys(cmd.params.at(0))
                );
            }

            static std::string Type(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                return Formatter::Format(
                    core->GetStorage()->Type(cmd.params.at(0))
                );
            }

            static std::string TTL(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                return Formatter::Format(
                    core->GetStorage()->TTL(cmd.params.at(0))
                );
            }

            static std::string Expire(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 2){
                    return Formatter::ErrorParams(cmd);
                }
                uint64_t ttl;
                try{
                    ttl = std::stoi(cmd.params.at(1));
                }catch(...){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                return Formatter::Format(
                    core->GetStorage()->Expire(cmd.params.at(0), ttl)
                );
            }

            static std::string LGet(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->Get<Lemis::ListValue>(cmd.params.at(0));
                if(output.has_value()){
                    return Formatter::Format(output.value());
                }
                return Formatter::Nil();
            }

            static std::string LDel(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);                    
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->LDel(cmd.params.at(0));
                return Formatter::Format(output);
            }

            static std::string LPush(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() < 2){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                size_t output = 0;
                for(size_t i = 1; i < cmd.params.size(); i++){
                    output = core->GetStorage()->LPush(cmd.params.at(0), cmd.params.at(i));
                }
                return Formatter::Format(output);    
            }

            static std::string LRem(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() < 2){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                size_t output = 0;
                for(size_t i = 1; i < cmd.params.size(); i++){
                    output += core->GetStorage()->LRem(cmd.params.at(0), cmd.params.at(i));
                }                
                return Formatter::Format(output);    
            }

            static std::string LIndex(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 2){
                    return Formatter::ErrorParams(cmd);  
                }
                size_t index;
                try{
                    index = std::stoi(cmd.params.at(1));
                }catch(...){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->LIndex(cmd.params.at(0), index);
                if(output.has_value()){
                    return Formatter::Format(output.value());
                }
                return Formatter::Nil();
            }

            static std::string LSet(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 3){
                    return Formatter::ErrorParams(cmd);  
                }
                size_t index;
                try{
                    index = std::stoi(cmd.params.at(1));
                }catch(...){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto result = core->GetStorage()->LSet(cmd.params.at(0), index, cmd.params.at(2));
                return result ? Formatter::Ok() : Formatter::Nil();
            }

            static std::string LLen(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->LLen(cmd.params.at(0));
                return Formatter::Format(output);    
            }

            static std::string LRange(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 3){
                    return Formatter::ErrorParams(cmd);  
                }
                int64_t from, to;
                try{
                    from = std::stoi(cmd.params.at(1));
                    to = std::stoi(cmd.params.at(2));
                }catch(...){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->LRange(cmd.params.at(0), from, to);
                return Formatter::Format(output);    
            }

            static std::string SAdd(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() < 2){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                size_t output = 0;
                for(size_t i = 1; i < cmd.params.size(); i++){
                    output += core->GetStorage()->SAdd(cmd.params.at(0), cmd.params.at(i));
                }
                return Formatter::Format(output);    
            }

            static std::string SGet(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->Get<Lemis::SetValue>(cmd.params.at(0));
                if(output.has_value()){
                    return Formatter::Format(output.value());
                }
                return Formatter::Nil();
            }

            static std::string SRem(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() < 2){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                size_t output = 0;
                for(size_t i = 1; i < cmd.params.size(); i++){
                    output += core->GetStorage()->SRem(cmd.params.at(0), cmd.params.at(i));
                }
                return Formatter::Format(output);    
            }

            static std::string SIsMember(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 2){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->SIsMember(cmd.params.at(0), cmd.params.at(1));
                return Formatter::Format(output);    
            }

            static std::string SMove(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 3){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->SMove(cmd.params.at(0), cmd.params.at(1), cmd.params.at(2));
                return Formatter::Format(output);    
            }

            static std::string SDel(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);                    
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->SDel(cmd.params.at(0));
                return Formatter::Format(output);
            }

            static std::string SLen(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);  
                }
                std::unique_lock<std::mutex> lock(core->GetStorage()->GetLock(cmd.params.at(0)));
                auto output = core->GetStorage()->SLen(cmd.params.at(0));
                return Formatter::Format(output);    
            }

            static std::string FlushAll(Lemis::Core* core, Lemis::Command& cmd){
                if(cmd.params.size() != 0){
                    return Formatter::ErrorParams(cmd);     
                }
                return Formatter::Format(
                    core->GetStorage()->FlushAll()
                );
            }

            static std::string Ping(Lemis::Core*, Lemis::Command&){
                return Formatter::Pong();      
            }

            static std::string Echo(Lemis::Core*, Lemis::Command& cmd){
                if(cmd.params.size() != 1){
                    return Formatter::ErrorParams(cmd);
                }
                return Formatter::Echo(
                    cmd.params.at(0)
                ); 
            }

            static std::string Info(Lemis::Core*, Lemis::Command& cmd){
                if(cmd.params.size() != 0){
                    return Formatter::ErrorParams(cmd);     
                }
                return Formatter::Format(std::vector<std::string>{
                    "# Server",
                    "redis_version:Lemis 1.0",
                    ""
                });
            }

            static std::string Quit(Lemis::Core*, Lemis::Command& cmd){
                if(cmd.params.size() != 0){
                    return Formatter::ErrorParams(cmd);     
                }
                return Formatter::Ok();
            }

            static std::string Scan(Lemis::Core* core, Lemis::Command& cmd){
                size_t from = 0;
                size_t to = from;
                try{
                    from = cmd.params.size() >= 1 ? std::stoi(cmd.params.at(0)) : from;
                }catch(...){}
                auto keys = core->GetStorage()->Keys();
                from = from > keys.size() ? keys.size() : from;
                to = from + 100;
                to = to > keys.size() ? keys.size() : to;
                std::vector<std::string> output_keys(keys.begin() + from, keys.begin() + to);
                return Formatter::FormatScan(to, output_keys);
            }

            static std::string Commands(Lemis::Core*, Lemis::Command& cmd){
                if(cmd.params.size() != 0){
                    return Formatter::ErrorParams(cmd);     
                }
                std::vector<std::string> commands;
                for(auto& pair : Mapper::functions){
                    commands.push_back(pair.first);
                }
                return Formatter::Format(commands);
            }
    };

    std::unordered_map<std::string, Function> Mapper::functions = 
    {
        { "get", Get },
        { "set", Set },
        { "del", Del },
        { "copy", Copy },
        { "append", Append },
        { "exists", Exists },
        { "keys", Keys },
        { "type", Type },
        { "ttl", TTL },
        { "expire", Expire },

        { "lget", LGet },
        { "ldel", LDel },
        { "lpush", LPush },
        { "lrem", LRem },
        { "lindex", LIndex },
        { "lset", LSet },
        { "llen", LLen },
        { "lrange", LRange },

        { "sget", SGet },
        { "sadd", SAdd },
        { "srem", SRem },
        { "smembers", SGet },
        { "sismember", SIsMember },
        { "smove", SMove },
        { "sdel", SDel },
        { "slen", SLen },

        { "commands", Commands },
        { "ping", Ping },
        { "flushall", FlushAll },
        { "echo", Echo },
        { "info", Info },
        { "quit", Quit },

        { "scan", Scan },
    };
}
