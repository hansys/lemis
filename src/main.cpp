#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>

#include "./lemis/core.h"
#include "./resp2/protocol.h"
#include "./lemis/server.h"
#include "./lemis/configParser.h"

int main(int argc, char** argv)
{
    auto config = Lemis::ConfigParser::ParseFile(argc >= 2 ? argv[1] : "/etc/lemis/lemis.conf");
    auto core = std::make_shared<Lemis::Core>();
    auto protocol = std::make_unique<Resp2::Protocol>(core);

    auto server = Lemis::Server(
        std::move(protocol),
        config,
    );

    server.Load();
    server.Start();
    return 0;
}
