// This tells doctest to provide a main() function
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <algorithm>
#include "../libs/doctest/doctest.h"
#include "./lemis/core.h"
#include "./resp2/protocol.h"
#include "./lemis/server.h"

TEST_SUITE("Lemis Core - Storage") {
    TEST_CASE("Storage Types") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core.get()->GetStorage();

        storage->Set("string_key","value");
        REQUIRE(storage->Get("string_key").value()->GetType() == Lemis::ValueType::String);

        storage->Set("int_key", 2);
        REQUIRE(storage->Get("int_key").value()->GetType() == Lemis::ValueType::Number);

        storage->Set("double_key", 2.5);
        REQUIRE(storage->Get("double_key").value()->GetType() == Lemis::ValueType::Number);

        storage->Set("bool_key", true);
        REQUIRE(storage->Get("bool_key").value()->GetType() == Lemis::ValueType::Number);

    }

    TEST_CASE("Storage SET/EXISTS/GET/DEL") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core.get()->GetStorage();

        REQUIRE(storage->Set("key","value") == true);
        REQUIRE(storage->Exists("key") == true);

        auto value = storage->Get("key");
        REQUIRE(value.has_value());
        REQUIRE(value.value()->ToString() == "value");

        REQUIRE(storage->Exists("key") == true);
        REQUIRE(storage->Del("key") == true);
    }

    TEST_CASE("Storage Keys") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core.get()->GetStorage();

        std::vector<std::string> keys = {"key1", "key2","key3"};
        std::sort(keys.begin(), keys.end());
        for(auto key : keys){
            storage->Set(key, "value");
        }

        auto storage_keys = storage->Keys();
        std::sort(storage_keys.begin(), storage_keys.end());
        REQUIRE(storage_keys.at(0) == keys.at(0));
        REQUIRE(storage_keys.at(1) == keys.at(1));
        REQUIRE(storage_keys.at(2) == keys.at(2));
    }

    TEST_CASE("Storage FlushAll") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core.get()->GetStorage();
        storage->Set("key1","value");
        storage->Set("key2","value");
        storage->FlushAll();
        REQUIRE(storage->Exists("key1") == false);
        REQUIRE(storage->Exists("key2") == false);
    }
}   


TEST_SUITE("Lemis Protocol - RESP2") {
    TEST_CASE("Parse correct command") {
        auto core = std::make_shared<Lemis::Core>();
        auto protocol = std::make_shared<Resp2::Protocol>(core);
        std::string input = "*3\r\n$3\r\nset\r\n$3\r\nkey\r\n$5\r\nvalue\r\n";
        auto command = protocol.get()->Parse(input);
        REQUIRE(command.name == "set");
        REQUIRE(command.params[0] == "key");
        REQUIRE(command.params[1] == "value");
    }

    TEST_CASE("Parse incorrect command") {
        auto core = std::make_shared<Lemis::Core>();
        auto protocol = std::make_shared<Resp2::Protocol>(core);
        std::string input = "*3\r\n$1\r\nset\r\n$3\r\nkey\r\n$5\r\nvalue\r\n";
        auto command = protocol.get()->Parse(input);
        REQUIRE(command.name == "s");
    }

    TEST_CASE("Unknown command") {
        auto core = std::make_shared<Lemis::Core>();
        auto protocol = std::make_shared<Resp2::Protocol>(core);

        std::string input = "*3\r\n$3\r\nbad\r\n$3\r\nkey\r\n$5\r\nvalue\r\n";
        auto command = protocol.get()->Parse(input);
        auto result = protocol.get()->Exec(command);
        REQUIRE(result.substr(0, 4) == "-ERR");
    }

    TEST_CASE("Set command - Missing parameters") {
        auto core = std::make_shared<Lemis::Core>();
        auto protocol = std::make_shared<Resp2::Protocol>(core);

        std::string input = "*2\r\n$3\r\nset\r\n$3\r\nkey\r\n";
        auto command = protocol.get()->Parse(input);
        auto result = protocol.get()->Exec(command);
        REQUIRE(result.substr(0, 4) == "-ERR");
    }

    TEST_CASE("Set command") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core->GetStorage();
        auto protocol = std::make_shared<Resp2::Protocol>(core);
        std::string input = "*3\r\n$3\r\nset\r\n$3\r\nkey\r\n$5\r\nvalue\r\n";
        auto command = protocol.get()->Parse(input);
        auto result = protocol.get()->Exec(command);
        REQUIRE(result == ":1\r\n");
        REQUIRE(storage->Get("key").has_value());
        REQUIRE(storage->Get("key").value()->ToString() == "value");
    }

    TEST_CASE("Get command") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core->GetStorage();
        auto protocol = std::make_shared<Resp2::Protocol>(core);

        std::string input = "*2\r\n$3\r\nget\r\n$3\r\nkey\r\n";
        auto command = protocol.get()->Parse(input);

        auto result1 = protocol.get()->Exec(command);
        REQUIRE(result1 == "$-1\r\n");

        storage->Set("key","value");
        auto result2 = protocol.get()->Exec(command);
        REQUIRE(result2 == "$5\r\nvalue\r\n");
    }

    TEST_CASE("Exists command") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core->GetStorage();
        auto protocol = std::make_shared<Resp2::Protocol>(core);

        std::string input = "*2\r\n$6\r\nexists\r\n$3\r\nkey\r\n";
        auto command = protocol.get()->Parse(input);

        auto result1 = protocol.get()->Exec(command);
        REQUIRE(result1 == ":0\r\n");

        storage->Set("key","value");
        auto result2 = protocol.get()->Exec(command);
        REQUIRE(result2 == ":1\r\n");
    }

    TEST_CASE("Del command") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core->GetStorage();
        auto protocol = std::make_shared<Resp2::Protocol>(core);

        std::string input = "*2\r\n$3\r\ndel\r\n$3\r\nkey\r\n";
        auto command = protocol.get()->Parse(input);

        auto result1 = protocol.get()->Exec(command);
        REQUIRE(result1 == ":0\r\n");

        storage->Set("key","value");
        auto result2 = protocol.get()->Exec(command);
        REQUIRE(result2 == ":1\r\n");
        REQUIRE(!storage->Exists("key"));
    }

    TEST_CASE("Copy command") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core->GetStorage();
        auto protocol = std::make_shared<Resp2::Protocol>(core);

        std::string input = "*3\r\n$4\r\ncopy\r\n$3\r\nkey\r\n$4\r\nkey2\r\n";
        auto command = protocol.get()->Parse(input);

        auto result1 = protocol.get()->Exec(command);
        REQUIRE(result1 == ":0\r\n");

        storage->Set("key","value");
        auto result2 = protocol.get()->Exec(command);
        REQUIRE(result2 == ":1\r\n");
        REQUIRE(storage->Get("key2").value()->ToString() == "value");
    }

    TEST_CASE("Append command") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core->GetStorage();
        auto protocol = std::make_shared<Resp2::Protocol>(core);

        std::string input = "*3\r\n$6\r\nappend\r\n$3\r\nkey\r\n$5\r\nvalue\r\n";
        auto command = protocol.get()->Parse(input);

        auto result1 = protocol.get()->Exec(command);
        REQUIRE(result1 == ":5\r\n");

        storage->Set("key","value");
        auto result2 = protocol.get()->Exec(command);
        REQUIRE(result2 == ":10\r\n");
        REQUIRE(storage->Get("key").value()->ToString() == "valuevalue");
    }

    TEST_CASE("Type command") {
        auto core = std::make_shared<Lemis::Core>();
        auto* storage = core->GetStorage();
        auto protocol = std::make_shared<Resp2::Protocol>(core);

        std::string input = "*2\r\n$4\r\ntype\r\n$3\r\nkey\r\n";
        auto command = protocol.get()->Parse(input);

        auto result1 = protocol.get()->Exec(command);
        REQUIRE(result1 == "$4\r\nnone\r\n");

        storage->Set("key","value");
        auto result2 = protocol.get()->Exec(command);
        REQUIRE(result2 == "$6\r\nstring\r\n");
    }
}