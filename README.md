<img align="right" height="200" src="logo.png" alt="Lemis logo">

# Lemis
#### The key-value in-memory database.
Lemis is a key-value in-memory database with Redis compatible command-line and RESP2 protocol support. 
The goal is to be as compatible as possible. 

## Features
* Loading configuration file
* TCP communication by RESP2 protocol
* Basic logging
* Optional storing database on disk 
* Support of multiple data types: 
   * String
   * List (Collection of strings, sorted by insertion order) - *Optional feature*
   * Set (Unordered collection of strings) - *Optional feature*

 # Configuration file
 Lemis support loading configuration from provided configuration file. The custom configuration file is possible to supply as argument.
 ```
 /usr/bin/lemis /etc/lemis/lemis.conf
 ```
 Lemis will try to use default configuration file at location `/etc/lemis/lemis.conf` if no custom configuration file is presented. 
 The structure of configuration file is shown below. If no default configuration file exists, lemis will use the default preset configuration which is same as this one.
 
 ```
 # Accept connections on the specified port, default is 6379..
 port 6379
 
 # On specified interfaces will Lemis listen for incoming connections. Multiple values separated by space are supported.
 bind 127.0.0.1 ::1
 
 # Close the connection after a client is idle for X seconds (0 to disable).
 timeout 300
 
 # Lemis verbosity of logging. Possible values are: notice, error.
 loglevel notice
 
 # The directory for logs. The log file lemis.log will be created.
 logdir /var/log/lemis/
 
 # The directory for database data on disk.
 dir /var/lib/lemis/
 
 # Require clients to issue AUTH <PASSWORD> before processing any other commands. 
 # No password is required if no requirepass directive or blank password is preseted.
 requirepass mysecretpassword
 
 # Lemis will save the database to disk if the given number of seconds occurred.
 save 900
 ```
 
## Supported commands
### System commands
| Command | Returns | Description | Detailed description | 
| ------ | ------ | ------ | ------ |
| ping | pong | Server status check | [ping](https://redis.io/commands/ping) |
| flushall | ok | Deletes all keys | [fluashall](https://redis.io/commands/fluashall) |
| auth {password} | ok | Authenticates the current connection  | [auth](https://redis.io/commands/auth) |
| quit | ok | Asks server to close connection | [quit](https://redis.io/commands/quit) |
| info | Bulk String | Returns various server information | [info](https://redis.io/commands/info) |
| client setname {connection-name} | ok | Returns various server information | [client-setname](https://redis.io/commands/client-setname) |
| echo {message} | {message} | Assigns a name to the current connection | [echo](https://redis.io/commands/echo) |

### String data type commands - *Optional feature*
| Command | Returns | Description | Detailed description | 
| ------ | ------ | ------ | ------ |
| set {key} {value} | ok/nil | Inserts new key {key} with value {value}. Returns ok on success and nil on failure. | [set](https://redis.io/commands/set) |
| get {key} | {value}/nil | Returns value of key {key} or nil as null. | [get](https://redis.io/commands/get) |
| del {key} | Integer | Deletes key {key} and returns number of removed keys. | [del](https://redis.io/commands/del) |
| exists {key} .. | Integer | Returns count of existing keys from the specified input | [exists](https://redis.io/commands/exists) |
| copy {copied_key} {new_key} | Integer | Creates copy of {key} with new name {new_key}. Returns 1 on success and 0 on failure.  | [copy](https://redis.io/commands/copy) |
| append {key} {value} | Integer | Append new value to {key} value. Returns the length of the string after the append operation. | [append](https://redis.io/commands/append) |

### List data type commands - *Optional feature*
| Command | Returns | Description | Detailed description | 
| ------ | ------ | ------ | ------ |
| lpush {key} {value} | Integer | Adds new value {value} to list with key {key}. Returns the length of list after push operation. | [lpush](https://redis.io/commands/lpush) |
| lrem {key} {value} | Integer | Removes value {value} from list with key {key}. Returns the number of removed elements. | [lrem](https://redis.io/commands/lrem) |
| lindex {key} {index} | {value}/nil | Returns value on index {index} from list with key {key} or nil on null. | [lindex](https://redis.io/commands/lindex) |
| lset {key} {index} {value} | ok/nil | Save value {value} to list with key {key}. Returns ok on success and nil on failure.  | [lset](https://redis.io/commands/lset) |

### Set data type commands
| Command | Returns | Description | Detailed description | 
| ------ | ------ | ------ | ------ |
| sadd {key} {value} | Integer | Adds new value {value} to set with key {key} and returns the number of elements that were added to the set. | [sadd](https://redis.io/commands/sadd) |
| srem {key} {value} | Integer | Removes value {value} from set with key {key} and returns the number of members that were removed from the set. | [srem](https://redis.io/commands/srem) |
| sismember {key} {value} .. | Integer | Returns 1 if item is member of set with key {key} and 0 if not. | [sismember](https://redis.io/commands/sismember) |
| smove {old_set_key} {new_set_key} {value} .. | Integer | Returns 0 on error and 1 if item is moved. | [smove](https://redis.io/commands/smove) |

> Commands should be possible to use via CLI or RESP2 protocol.

> An additional commands may be supported or at least simulated for fully functional work of Redis clients. 
> Features and commands will be tested against this client: [Another Redis Desktop Manager](https://github.com/qishibo/AnotherRedisDesktopManager).

## RESP2 protocol
Remis offers TCP communication via RESP2 protocol. RESP2 protocol consists of simple sequence of commands complemented by control symbols.
Once a command is received, it is processed and a reply is sent back to the client. All communication is human readable.
Detailed description of RESP2 protocol is avaiable [here](https://redis.io/topics/protocol).

## Missing features
* RESP2 Pipelining (It is possible for clients to send multiple commands at once, and wait for replies later.)
* Support of data types: 
   * ZSet (Collection of strings, sorted by element's score)
   * Hash (Maps between string fields and string values)
   * Stream (Append-only collections of map-like entries that provide an abstract log data type)

## Used libraries
For TCP communication will be Lemis using open-source [evpp](https://github.com/Qihoo360/evpp) library under BSD 3-Clause License.

## License
MIT
